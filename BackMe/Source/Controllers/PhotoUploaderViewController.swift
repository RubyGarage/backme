//
//  PhotoUploaderViewController.swift
//  BackMe
//
//  Created by Radislav Crechet on 6/15/17.
//  Copyright © 2017 RubyGarage. All rights reserved.
//

import UIKit

class PhotoUploaderViewController: UIViewController {
    
    // MARK: Outlets
    
    @IBOutlet var imageView: UIImageView!
    @IBOutlet var activityIndicator: UIActivityIndicatorView!
    @IBOutlet var uploadButton: UIBarButtonItem!
    
    // MARK: Properties
    
    fileprivate var imageUrl: URL?
    fileprivate var uploadTask: URLSessionUploadTask?
    
    fileprivate var imageName: String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd_HH:mm:ss"
        
        return dateFormatter.string(from: Date()).appending(".png")
    }
    
    // MARK: Lifecycle
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if uploadTask == nil && imageUrl == nil {
            clearUserInterface()
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        uploadTask?.cancel()
    }
    
    // MARK: Presentation
    
    private func presentImagePicker() {
        let imagePickerController = UIImagePickerController()
        imagePickerController.delegate = self
        imagePickerController.sourceType = .photoLibrary
        present(imagePickerController, animated: true, completion: nil)
    }
    
    fileprivate func clearUserInterface() {
        imageView.image = nil
        activityIndicator.stopAnimating()
        uploadButton.isEnabled = false
    }
    
    // MARK: Actions
    
    @IBAction func chooseButtonPressed(_ sender: UIBarButtonItem) {
        presentImagePicker()
    }
    
    @IBAction func uploadButtonPressed(_ sender: UIBarButtonItem) {
        uploadImage()
    }
    
}

// MARK: -

extension PhotoUploaderViewController {
    
    // MARK: Work With Image
    
    fileprivate func uploadImage() {
        activityIndicator.startAnimating()
        uploadButton.isEnabled = false
        
        uploadTask = NetworkService.uploadImage(withName: imageName, url: imageUrl!, sessionDelegate: self)
    }
    
}

// MARK: -

extension PhotoUploaderViewController: URLSessionTaskDelegate {
    
    // MARK: URLSessionTaskDelegate
    
    func urlSessionDidFinishEvents(forBackgroundURLSession session: URLSession) {
        DispatchQueue.main.async {
            self.imageUrl = nil
            self.uploadTask = nil
            
            let applicationDelegate = UIApplication.shared.delegate as! AppDelegate
            applicationDelegate.completionHandler!()
            applicationDelegate.completionHandler = nil
        }
    }
    
    func urlSession(_ session: URLSession, task: URLSessionTask, didCompleteWithError error: Error?) {
        DispatchQueue.main.async {
            self.imageUrl = nil
            self.uploadTask = nil
            
            self.clearUserInterface()
        }
    }
    
}

// MARK: -

extension PhotoUploaderViewController: UINavigationControllerDelegate, UIImagePickerControllerDelegate {
    
    // MARK: UIImagePickerControllerDelegate
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        let image = info[UIImagePickerControllerOriginalImage] as! UIImage
        imageUrl = info[UIImagePickerControllerImageURL] as? URL
        
        uploadButton.isEnabled = true
        imageView.image = image
        
        dismiss(animated: true)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true)
    }
    
}
