//
//  WeatherForecastViewController.swift
//  BackMe
//
//  Created by Radislav Crechet on 6/14/17.
//  Copyright © 2017 RubyGarage. All rights reserved.
//

import UIKit

class WeatherForecastViewController: UIViewController {
    
    // MARK: Outlets
    
    @IBOutlet var forecastLabel: UILabel!
    
    // MARK: Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()

        forecastLabel.text = String(forecast)
    }
    
    // MARK: Actions
    
    @IBAction func updateButtonPressed(_ sender: UIButton) {
        loadForecast()
    }
    
}

// MARK: -

extension WeatherForecastViewController {
    
    // MARK: Work With Forecast
    
    fileprivate var forecast: Int {
        return UserDefaults.standard.integer(forKey: "forecast")
    }
    
    func loadForecast() {
        NetworkService.forecast { [unowned self] forecast in
            guard let forecast = forecast else {
                return
            }
            
            self.setForecast(forecast)
            self.forecastLabel.text = String(forecast)
        }
    }
    
    fileprivate func setForecast(_ forecast: Int) {
        UserDefaults.standard.set(forecast, forKey: "forecast")
    }
    
}
