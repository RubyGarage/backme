//
//  VideoCameraViewController.swift
//  BackMe
//
//  Created by Radislav Crechet on 6/14/17.
//  Copyright © 2017 RubyGarage. All rights reserved.
//

import UIKit
import MobileCoreServices

class VideoCameraViewController: UIViewController {
    
    // MARK: Outlets
    
    @IBOutlet var captureVideoButton: UIButton!
    @IBOutlet var saveToCameraRollVideoButton: UIButton!
    @IBOutlet var activityIndicator: UIActivityIndicatorView!
    
    // MARK: Properties
    
    var timer: Timer?
    
    fileprivate var videoUrl: URL!
    fileprivate var backgroundTask = UIBackgroundTaskInvalid
    
    // MARK: Lifecycle
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        timer?.invalidate()
        timer = nil
    }
    
    // MARK: Presentation
    
    private func presentImagePicker() {
        let imagePickerController = UIImagePickerController()
        imagePickerController.delegate = self
        imagePickerController.sourceType = .camera
        imagePickerController.mediaTypes = [String(kUTTypeMovie)];
        imagePickerController.videoExportPreset = "AVAssetExportPresetHighestQuality"
        imagePickerController.videoMaximumDuration = 30.0
        present(imagePickerController, animated: true, completion: nil)
    }
    
    // MARK: Actions
    
    @IBAction func captureVideoButtonPressed(_ sender: UIButton) {
        presentImagePicker()
    }
    
    @IBAction func saveToCameraRollButtonPressed(_ sender: UIButton) {
        captureVideoButton.isEnabled = false
        saveToCameraRollVideoButton.isEnabled = false
        activityIndicator.startAnimating()
        
        timer = Timer.scheduledTimer(timeInterval: 5.0,
                                     target: self,
                                     selector: #selector(saveVideoToCameraRoll),
                                     userInfo: nil,
                                     repeats: false)
        
        beginBackgroundTask()
    }
    
}

// MARK: -

extension VideoCameraViewController {
    
    // MARK: Work With Background Task
    
    fileprivate func beginBackgroundTask() {
        backgroundTask = UIApplication.shared.beginBackgroundTask { [weak self] in
            self?.endBackgroundTask()
        }
    }
    
    fileprivate func endBackgroundTask() {
        UIApplication.shared.endBackgroundTask(backgroundTask)
        backgroundTask = UIBackgroundTaskInvalid
    }
    
}

// MARK: -

extension VideoCameraViewController {
    
    // MARK: Work With Video
    
    func saveVideoToCameraRoll() {
        UISaveVideoAtPathToSavedPhotosAlbum(videoUrl.path,
                                            self,
                                            #selector(video(videoPath:didFinishSavingWithError:contextInfo:)),
                                            nil)
    }
    
    func video(videoPath: NSString, didFinishSavingWithError error: NSError?, contextInfo info: AnyObject) {
        captureVideoButton.isEnabled = true
        saveToCameraRollVideoButton.isEnabled = true
        activityIndicator.stopAnimating()
        
        if backgroundTask != UIBackgroundTaskInvalid {
            endBackgroundTask()
        }
    }
    
}

// MARK: -

extension VideoCameraViewController: UINavigationControllerDelegate, UIImagePickerControllerDelegate {
    
    // MARK: UIImagePickerControllerDelegate
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        saveToCameraRollVideoButton.isEnabled = true
        videoUrl = info[UIImagePickerControllerMediaURL] as! URL
        
        dismiss(animated: true)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true)
    }
    
}
